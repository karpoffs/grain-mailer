package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func httpErrorJson(c *gin.Context, status int, message string) {
	c.AbortWithStatusJSON(status, gin.H{"status": "ERROR", "message": message})
}

func httpSuccessJson(c *gin.Context, payload interface{}) {
	c.AbortWithStatusJSON(http.StatusOK, gin.H{"status": "OK", "payload": payload})
}

func httpSuccessHtml(c *gin.Context, payload string) {
	c.Data(http.StatusOK, "text/html; charset=utf-8", []byte(payload))
	c.Abort()
}

func httpErrorHtml(c *gin.Context, status int, payload string) {
	c.Data(status, "text/html; charset=utf-8", []byte(payload))
	c.Abort()
}
