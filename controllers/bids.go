package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/database/boltdb"
	"gitlab.magic-egg.net/server/grain-mailer/domain"
	"gitlab.magic-egg.net/server/grain-mailer/service"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"net/http"
)

type BidsController struct{}

func (f *BidsController) SendBidsForAllUsers(c *gin.Context) {

	data, err := service.SendBidsEmail()
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessJson(c, data)
	return
}

func (f *BidsController) SendBidsByUser(c *gin.Context) {
	var user types.RequestUser
	if err := c.ShouldBindJSON(&user); err != nil {
		httpErrorJson(c, http.StatusBadRequest, err.Error())
		return
	}

	data, err := service.SendBidsByUser(user)
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessJson(c, data)
	return
}

func (f *BidsController) PreviewBidsByUser(c *gin.Context) {

	var user types.RequestUser
	if err := c.ShouldBindJSON(&user); err != nil {
		httpErrorHtml(c, http.StatusBadRequest, err.Error())
		return
	}

	htmlStr, err := service.PreviewBidsByUser(user)
	if err != nil {
		httpErrorHtml(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessHtml(c, htmlStr)
	return
}

func (f *BidsController) JsonBidsByUser(c *gin.Context) {

	var user types.RequestUser
	if err := c.ShouldBindJSON(&user); err != nil {
		httpErrorJson(c, http.StatusBadRequest, err.Error())
		return
	}

	usersEmail, usersSms, err := domain.GetDataByOneUser(user)
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	if len(usersEmail) + len(usersSms) == 0 {
		httpErrorJson(c, http.StatusInternalServerError, "Data not found")
		return
	}

	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessJson(c, usersEmail)
	return
}

func (f *BidsController) JsonBidsAllUsers(c *gin.Context) {

	_, users, err := domain.GetDataAllUsersFromApiService()
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessJson(c, users)
	return
}

func (f *BidsController) JsonBidsAllUsersFromDB(c *gin.Context) {

	localDB, err := boltdb.GetDb()
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	users, err := domain.GetUserListFromDB(localDB, types.USER_DB_BUCKET, types.ENCODE_TYPE_JSON)
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	httpSuccessJson(c, users)
	return
}
