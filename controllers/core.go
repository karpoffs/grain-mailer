package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/service"
	"net/http"
)

type CoreController struct{}

// Строка на корень
func (core CoreController) Hello(c *gin.Context) {

	htmlString, err := service.RenderTemplate("none.html", nil)
	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	// Отдаём html
	c.Data(http.StatusOK, "text/html; charset=utf-8", []byte(htmlString))

	//c.Set("template", "index.html")
	//c.Set("data", data)

	//c.String(http.StatusOK, "Hello Word!")
	c.Abort()
}
