package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/config"
)

type ConfigController struct{}

func (cc ConfigController) GetConfig(c *gin.Context) {
	httpSuccessJson(c, config.Get())
	return
}
