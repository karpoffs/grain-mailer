package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type PingController struct{}


func (h PingController) Pong(c *gin.Context) {
	c.String(http.StatusOK, "PONG")
	c.Abort()
}
