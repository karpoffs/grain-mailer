package controllers

import (
	"github.com/bamzi/jobrunner"
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/workers"
	"net/http"
)

type WorkerController struct{}

func (w *WorkerController) GetStatus(c *gin.Context) {
	httpSuccessJson(c, jobrunner.StatusJson())
	return
}

func (w *WorkerController) GetList(c *gin.Context) {
	httpSuccessJson(c, workers.GetWorkersCollection())
	return
}

func (w *WorkerController) RunJob(c *gin.Context) {

	name, ok := c.GetQuery("job")
	if !ok {
		httpErrorJson(c, http.StatusBadRequest, "Parameter job is not set!")
		return
	}

	worker, err := workers.GetWorker(name)
	if err != nil {
		httpErrorJson(c, http.StatusInternalServerError, err.Error())
		return
	}

	jobrunner.Now(worker)
	httpSuccessJson(c, nil)
	return
}
