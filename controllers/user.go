package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"net/http"
)

type UserController struct{}

func (u UserController) GetUsers(c *gin.Context) {
	httpSuccessJson(c, config.Get().Users)
	return
}

func (u UserController) GetUserByName(c *gin.Context) {
	conf := config.Get()
	user := c.Params.ByName("name")
	password, ok := conf.Users[user]
	if ok {
		httpSuccessJson(c, gin.H{"login": user, "password": password})
	} else {
		httpErrorJson(c, http.StatusInternalServerError, "user: "+user+" not found")
	}
	return
}
