package domain

import (
	"encoding/json"
	"fmt"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type route struct {
	Path   string
	Method string
}

var Routes = map[string]route{
	"login":          route{Path: "/api/user/login", Method: "POST"},
	"users":          route{Path: "/api/users", Method: "GET"},
	"bid_filters":    route{Path: "/api/bid_filters", Method: "GET"},
	"bids_type_best": route{Path: "/api/bids/{type}/best", Method: "POST"},
	"send_sms": 	  route{Path: "/api/send_sms", Method: "POST"},
	"settings":       route{Path: "/api/system/settings", Method: "GET"},
	"get_jwt":		  route{Path: "/api/get_jwt", Method: "GET"},
}

const api_token_header = "X-AUTH-TOKEN"

// Метод авторизаии для работы с API
func requestUserForLogin(user types.RequestUser) (types.User, error) {

	c := config.Get()
	urlString := c.Project.ApiBase + Routes["login"].Path

	form := url.Values{}
	form.Add("login", user.Login)
	form.Add("password", user.Password)

	req, _ := http.NewRequest(Routes["login"].Method, urlString, strings.NewReader(form.Encode()))

	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	//log.Printf("%s \n", req.URL.String())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return types.User{}, err
	}

	if res.Header.Get("Content-Type") == "application/json" {

		if res.StatusCode == http.StatusOK {
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			var response types.ApiResponseUser

			err := json.Unmarshal(body, &response)
			if err != nil {
				return types.User{}, err
			}

			return response.Data, nil
		} else {

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			var response types.ApiResponseError

			err := json.Unmarshal(body, &response)
			if err != nil {
				return types.User{}, err
			}

			return types.User{}, fmt.Errorf(response.Message)
		}
	}

	return types.User{}, nil
}


func RequestSendSmsToUser(user types.User, message string) (bool, error) {

	c := config.Get()
	urlString := c.Project.ApiBase + Routes["send_sms"].Path  

	req, _ := http.NewRequest(Routes["send_sms"].Method, urlString, nil)

	params := req.URL.Query()
	params.Add("phone", user.Phone)
	params.Add("message", message)
	req.URL.RawQuery = params.Encode()

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return false, err
	}

	if res.Header.Get("Content-Type") == "application/json" {
		if res.StatusCode == http.StatusOK {
			return true, nil
		} else {
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)
			var response types.ApiResponseError
			err := json.Unmarshal(body, &response)
			if err != nil {
				return false, err
			}
			return false, fmt.Errorf(response.Message)
		}
	}
	return true, nil
}

func RequestSettings() (types.Settings, error){
	var response types.ApiResponseSettings

	user := types.RequestUser{
		Login:    config.Get().Project.Auth.Login,
		Password: config.Get().Project.Auth.Password,
	}
	manageUser, err := requestUserForLogin(user)
	if err != nil {
		return response.Data, err
	}

	c := config.Get()
	urlString := c.Project.ApiBase + Routes["settings"].Path

	req, err := http.NewRequest(Routes["settings"].Method, urlString, nil)
	if err != nil {
		return response.Data, err
	}

	req.Header.Add(api_token_header, manageUser.APIToken)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return response.Data, err
	}

	if res.StatusCode == http.StatusOK && res.Header.Get("Content-Type") == "application/json" {
		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)

		errJson := json.Unmarshal(body, &response)
		if errJson != nil {
			return response.Data, errJson
		}
	}

	return response.Data, nil
}

// Запрос получения списка пользователей
func requestUsers(token string, page int) (types.ApiResponseUsers, error) {

	var response types.ApiResponseUsers

	c := config.Get()
	urlString := c.Project.ApiBase + Routes["users"].Path

	req, err := http.NewRequest(Routes["users"].Method, urlString, nil)
	if err != nil {
		return response, err
	}

	req.Header.Add(api_token_header, token)

	params := req.URL.Query()
	params.Add("page", strconv.FormatInt(int64(page), 10))
	req.URL.RawQuery = params.Encode()

	//log.Printf("%s \n", req.URL.String())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return response, err
	}

	if res.StatusCode == http.StatusOK && res.Header.Get("Content-Type") == "application/json" {
		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)

		errJson := json.Unmarshal(body, &response)
		if errJson != nil {
			return response, errJson
		}
	}

	return response, nil
}

func requestJWTToken(user types.User) (types.User, error) {

	var response types.ApiResponseToken

	c := config.Get()
	urlString := c.Project.ApiBase + Routes["get_jwt"].Path

	req, err := http.NewRequest(Routes["get_jwt"].Method, urlString, nil)
	if err != nil {
		return user, err
	}

	req.Header.Add(api_token_header, user.APIToken)

	//log.Printf("%s \n", req.URL.String())

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return user, err
	}

	if res.StatusCode == http.StatusOK && res.Header.Get("Content-Type") == "application/json" {
		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)

		errJson := json.Unmarshal(body, &response)
		if errJson != nil {
			return user, errJson
		}
	}
	user.JWTToken = response.Token

	return user, nil
}

// Запрос с для получения списка фильтров ползователя
func requestBidFiltersByUser(user *types.User, filterType string, page int) (types.ApiResponseBidFilter, error) {

	var response = types.ApiResponseBidFilter{}
	c := config.Get()
	urlString := c.Project.ApiBase + Routes["bid_filters"].Path

	req, err := http.NewRequest(Routes["bid_filters"].Method, urlString, nil)
	if err != nil {
		return response, err
	}

	req.Header.Add(api_token_header, user.APIToken)

	params := req.URL.Query()
	params.Add("page", strconv.FormatInt(int64(page), 10))
	params.Add("type", filterType)
	req.URL.RawQuery = params.Encode()

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return response, err
	}

	if res.StatusCode == http.StatusOK && res.Header.Get("Content-Type") == "application/json" {
		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)

		errJson := json.Unmarshal(body, &response)
		if errJson != nil {
			return response, err
		}
	}

	return response, nil
}

// Метод получения списка заявок по фильтру пользоваетля
func requestGetBidsBest(user types.User, filter types.BidFilter) (types.ApiResponseBidBest, error) {

	var response types.ApiResponseBidBest

	var page = 1

	c := config.Get()
	route := strings.Replace(Routes["bids_type_best"].Path, "{type}", filter.BidType, -1)
	urlString := c.Project.ApiBase + route

	payload := types.RequestBestBid{}
	payload.Filter.CropID = filter.Crop.ID
	payload.Filter.MaxDistance = filter.MaxDistance
	payload.Filter.MaxFullPrice = filter.MaxFullPrice
	payload.Filter.MinFullPrice = filter.MinFullPrice

	if len(filter.ParameterValues) > 0 {
		for _, parameterValue := range filter.ParameterValues {
			payload.Filter.ParameterValues = append(payload.Filter.ParameterValues, types.RequestBestBidParameterValue{
				ParameterID: parameterValue.Parameter.ID,
				Value:       parameterValue.Value,
			})
		}
	} else {
		payload.Filter.ParameterValues = make([]types.RequestBestBidParameterValue, 0)
	}

	if len(filter.PointPrices) > 0 {
		for _, priceValue := range filter.PointPrices {
			payload.Filter.PointPrices = append(payload.Filter.PointPrices, types.RequestBestBidPointPrice{
				PointId: priceValue.Point.ID,
				Price:   priceValue.Price,
			})
		}
	} else {
		payload.Filter.PointPrices = make([]types.RequestBestBidPointPrice, 0)
	}

	//strJson, err := json.MarshalIndent(payload, "", "  ")
	strJson, err := json.Marshal(payload)
	if err != nil {
		return response, err
	}

	req, err := http.NewRequest(Routes["bids_type_best"].Method, urlString, strings.NewReader(string(strJson)))
	if err != nil {
		return response, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add(api_token_header, user.APIToken)

	params := req.URL.Query()
	params.Add("page", strconv.FormatInt(int64(page), 10))
	params.Add("is_mailing", strconv.FormatInt(1, 10))
	req.URL.RawQuery = params.Encode()

	if c.Environment == "development" {
		log.Printf("%s \n", req.URL.String())
		log.Println(string(strJson))
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return response, err
	}

	if res.StatusCode == http.StatusOK && res.Header.Get("Content-Type") == "application/json" {
		body, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()

		errJson := json.Unmarshal(body, &response)
		if errJson != nil {
			return response, err
		}
	}

	return response, nil
}
