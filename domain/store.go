package domain

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"gitlab.magic-egg.net/server/grain-mailer/database/boltdb"
	"gitlab.magic-egg.net/server/grain-mailer/types"
)

func GetUserListFromDB(localDB *boltdb.LocalDbType, bucket string, encodeType string) ([]types.User, error) {

	var res []types.User

	byteMap := map[string][]byte{}
	err := localDB.GetBucketList(bucket, byteMap)
	if err != nil {
		return nil, err
	}
	for _, v := range byteMap {
		user := types.User{}
		switch encodeType {
		case types.ENCODE_TYPE_JSON:
			err := json.Unmarshal(v, &user)
			if err != nil {
				return nil, err
			}
		case types.ENCODE_TYPE_GOB:
			buf := bytes.NewBuffer(v)
			dec := gob.NewDecoder(buf)
			err := dec.Decode(&user)
			if err != nil {
				return nil, err
			}
		}
		res = append(res, user)
	}
	return res, nil
}

func DelUserListFromDB(localDB *boltdb.LocalDbType, bucket string, key string) (bool, error) {
	err := localDB.Delete(bucket, key)
	if err != nil {
		return false, err
	}
	return true, nil
}
