package domain

import (
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"log"
	"strconv"
)

type DataUser struct {
	Users        map[string]types.User
	CountBidBest map[string]int
}

// Получение списка пользователей
func GetUsersMap(user types.User) (map[string]types.User, error) {

	var users = make(map[string]types.User)

	var page = 1

	request, err := requestUsers(user.APIToken, page)
	if err != nil {
		return users, err
	}

	// добавим управляющего пользователя
	users[strconv.Itoa(int(user.ID))] = user

	for _, user := range request.Data {
		uKey := strconv.Itoa(int(user.ID))
		users[uKey] = user
	}

	if request.TotalPages > page {
		for i := page + 1; i <= request.TotalPages; i++ {
			request, err := requestUsers(user.APIToken, i)
			if err != nil {
				return nil, err
			}

			for _, user := range request.Data {
				user, _ := requestJWTToken(user)
				uKey := strconv.Itoa(int(user.ID))
				users[uKey] = user
			}
		}
	}

	return users, nil
}

// Получение списка фильтров для email и sms рассылок
func GetBidFiltersByUserMap(user *types.User, filterType string) ([]types.BidFilter, []types.BidFilter) {

	var resultEmail []types.BidFilter
	var resultSms []types.BidFilter

	var page = 1

	request, _ := requestBidFiltersByUser(user, filterType, page)

	for _, filter := range request.Data.Filters {
		if filter.Subscribed == true {
			resultEmail = append(resultEmail, filter)
		}
		if filter.IsSendingSms == true {
			resultSms = append(resultSms, filter)
		}
	}

	if request.TotalPages > page {
		for i := page + 1; i <= request.TotalPages; i++ {
			request, _ := requestBidFiltersByUser(user, filterType, i)
			for _, filter := range request.Data.Filters {
				if filter.Subscribed == true {
					resultEmail = append(resultEmail, filter)
				}
				if filter.IsSendingSms == true {
					resultSms = append(resultSms, filter)
				}
			}
		}
	}

	return resultEmail, resultSms
}

// GetFiltersByUser Метод получения списка фильтров
func GetFiltersByUser(user *types.User, filterType string) ([]types.BidFilter, []types.BidFilter) {
	filtersEmail, filtersSms := GetBidFiltersByUserMap(user, filterType)
	return filtersEmail, filtersSms
}

// GetDataAllUsersFromApiService Метод получения списка пользователй
func GetDataAllUsersFromApiService() ([]types.User, []types.User, error) {

	user := types.RequestUser{
		Login:    config.Get().Project.Auth.Login,
		Password: config.Get().Project.Auth.Password,
	}
	manageUser, err := requestUserForLogin(user)
	log.Printf("User for login with id %d, email %s\n", manageUser.ID, manageUser.Email)
	if err != nil {
		return nil, nil, err
	}

	users, errU := GetUsersMap(manageUser)
	if errU != nil {
		return nil, nil, errU
	}

	return prepareFlatUsersByFilter(users)
}

func GetDataByOneUser(user types.RequestUser) ([]types.User, []types.User, error) {

	manageUser, err := requestUserForLogin(user)
	if err != nil {
		return nil, nil, err
	}

	var users = make(map[string]types.User)

	users[strconv.Itoa(int(manageUser.ID))] = manageUser

	return prepareFlatUsersByFilter(users)
}

func prepareFlatUsersByFilter(users map[string]types.User) ([]types.User, []types.User, error) {

	//maxCountBids := int(config.Get().Project.CountBestBids - 1)

	var actualFiltersEmail = make(map[string]int)
	var actualFiltersSms = make(map[string]int)

	// Инициализируем карту userKey|filtersTypes
	var mapFiltersEmail = make(map[string]types.BidFiltersMap)
	var mapFiltersSms = make(map[string]types.BidFiltersMap)

	// пробегаемся по пользователям
	for userKey, user := range users {

		// создаём второй слайс карты
		mapFiltersEmail[userKey] = make(types.BidFiltersMap)
		mapFiltersSms[userKey] = make(types.BidFiltersMap)

		// Получаем список типов фильтрова от типа пользователя
		for _, filterType := range user.GetFilterTypes() {
			filtersEmail, filtersSms := GetFiltersByUser(&user, filterType)

			for _, filter := range filtersEmail {
				if bestBids, err := requestGetBidsBest(user, filter); err == nil {

					var equals []types.Equal
					/*for _, equal := range bestBids.Data.Equal {
						//if i <= maxCountBids {
						equals = append(equals, equal)
						//}
					}*/

					//только по одному объявлению по каждому фильтру
					if len(bestBids.Data.Equal) > 0 {
						equals = append(equals, bestBids.Data.Equal[0])
					}

					if len(equals) > 0 {
						log.Printf(
							"User %s (%s) filter for Email id: %d - BidBests Equals %d \n",
							userKey, user.Email, filter.ID, len(equals))

						actualFiltersEmail[userKey] = actualFiltersEmail[userKey] + 1
						filter.BidBests = append(filter.BidBests, types.BidBest{Equal: equals})
						mapFiltersEmail[userKey][filterType] = append(mapFiltersEmail[userKey][filterType], filter)
					}
				}
			}

			for _, filter := range filtersSms {
				if bestBids, err := requestGetBidsBest(user, filter); err == nil {
					var equals []types.Equal
					/*for _, equal := range bestBids.Data.Equal {
						//if i <= maxCountBids {
						equals = append(equals, equal)
						//}
					}*/

					//только по одному объявлению по каждому фильтру
					if len(bestBids.Data.Equal) > 0 {
						equals = append(equals, bestBids.Data.Equal[0])
					}

					if len(equals) > 0 {
						log.Printf(
							"User %s (%s) filter for SMS id: %d, filter crop is %d - BidBests Equals %d \n",
							userKey, user.Email, filter.ID, filter.Crop.ID, len(equals))

						actualFiltersSms[userKey] = actualFiltersSms[userKey] + 1

						filter.BidBests = append(filter.BidBests, types.BidBest{Equal: equals})

						mapFiltersSms[userKey][filterType] = append(mapFiltersSms[userKey][filterType], filter)
					}
				}
			}
		}
	}

	var mapUsersEmail []types.User
	for userKey, user := range users {
		if actualFiltersEmail[userKey] > 0 {
			user.BidFilters = mapFiltersEmail[userKey]
			mapUsersEmail = append(mapUsersEmail, user)
		}
	}
	var mapUsersSms []types.User
	for userKey, user := range users {
		if actualFiltersSms[userKey] > 0 {
			user.BidFilters = mapFiltersSms[userKey]
			mapUsersSms = append(mapUsersSms, user)
		}
	}

	return mapUsersEmail, mapUsersSms, nil
}
