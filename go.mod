module gitlab.magic-egg.net/server/grain-mailer

go 1.14

require (
	github.com/bamzi/jobrunner v1.0.0
	github.com/boltdb/bolt v1.3.1
	github.com/flosch/pongo2 v0.0.0-20200509134334-76fc00043fe1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/configor v1.2.0
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jinzhu/gorm v1.9.12
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mailgun/mailgun-go/v4 v4.1.0
	github.com/robfig/cron v1.2.0
	gopkg.in/go-playground/validator.v8 v8.18.2
	gorm.io/gorm v1.23.5 // indirect
)
