package main

import (
	"context"
	"encoding/gob"
	"flag"
	"fmt"
	"github.com/bamzi/jobrunner"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/database/boltdb"
	"gitlab.magic-egg.net/server/grain-mailer/router"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"gitlab.magic-egg.net/server/grain-mailer/workers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime/debug"
	"strconv"
	"syscall"
	"time"
)

//var (
//	cacheDb *boltdb.LocalDbType
//)

func main() {

	gob.Register(&types.User{})

	debug.SetGCPercent(400)

	// Объявляем флаги
	configFilePath := flag.String("c", "config/config.yml", "config/config.yml")
	environment := flag.String("e", "development", "development|release")
	verbose := flag.Bool("v", false, "")

	// Описание как пользоватся
	flag.Usage = func() {
		fmt.Println("Usage: server -c {path to file}")
		fmt.Println("              -e {mode}")
		fmt.Println("              -v")
		os.Exit(1)
	}
	flag.Parse()

	// Инициализируем настройку
	config.Init(*environment, *verbose, *configFilePath)

	// Загружаем конфик
	c := config.Get()

	// Инициализируем boltdb
	cacheDb, err := boltdb.ConnectDb()
	if err != nil {
		log.Fatalf("%s\n", err)
	}
	//cacheDb, _ = boltdb.GetDb()
	defer cacheDb.Close()

	//database.Init()
	//db := database.GetDB()
	//defer db.Close()

	// optional: jobrunner.Start(pool int, concurrent int) (10, 1)
	jobrunner.Start(10, 10)

	// Задём запуск по расписанию
	for _, w := range c.Workers {
		worker, err := workers.GetWorker(w.Worker)
		if err != nil {
			log.Println(err)
		} else {
			// Time set example https://github.com/robfig/cron/blob/v2/doc.go
			if err := jobrunner.Schedule(w.Time, worker); err != nil {
				log.Println(err)
			}
		}
	}

	var address = c.Server.Address + ":" + strconv.FormatUint(uint64(c.Server.Port), 10)

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	srv := &http.Server{
		Addr:         address,
		Handler:      router.NewRouter(c), // маршруты
		ReadTimeout:  c.Server.ReadTimeout * time.Second,
		WriteTimeout: c.Server.WriteTimeout * time.Second,
	}
	go func() {
		log.Println("Starting Server - " + address)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	interruptChan := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(interruptChan, syscall.SIGKILL, syscall.SIGINT, syscall.SIGTERM)
	<-interruptChan
	log.Println("Shutting down server...")

	// Create a deadline to wait for.
	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
	os.Exit(0)
}
