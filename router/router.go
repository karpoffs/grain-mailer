package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin/binding"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/controllers"
	"gitlab.magic-egg.net/server/grain-mailer/middlewares"
	"gitlab.magic-egg.net/server/grain-mailer/service"
	_ "gopkg.in/go-playground/validator.v8"
	"time"
)

func NewRouter(c *config.Config) *gin.Engine {

	if c.Environment != "development" {
		gin.SetMode(gin.ReleaseMode)
	}

	// Disable Console Color
	//gin.DisableConsoleColor()
	router := gin.Default()

	// Устанвливаем шаблонизатор
	router.Use(service.Pongo2())

	// LoggerWithFormatter middleware will write the logs to gin.DefaultWriter
	// By default gin.DefaultWriter = os.Stdout
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))

	router.Use(gin.Recovery())

	// Register validations
	//if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
	//v.RegisterStructValidation(validators.LangStructLevelValidation, models.Lang{})
	//}

	// Core Hello
	core := new(controllers.CoreController)
	router.GET("/", core.Hello)

	// Ping test
	ping := new(controllers.PingController)
	router.GET("/ping", ping.Pong)

	// Подключаем обработку авторизации
	if len(c.Users) > 0 {
		router.Use(middlewares.AuthMiddleware(&c.Users))
	}

	api := router.Group("/api/")
	{
		send := new(controllers.BidsController)
		api.POST("/bids/preview/html", send.PreviewBidsByUser)
		api.POST("/bids/send/users", send.SendBidsForAllUsers)
		api.POST("/bids/send/by/user", send.SendBidsByUser)
		api.POST("/bids/get/json", send.JsonBidsByUser)
		api.GET("/bids/get/json/all", send.JsonBidsAllUsers)
		api.GET("/bids/get/json/db", send.JsonBidsAllUsersFromDB)
	}

	system := router.Group("/system/")
	{
		// Config
		sc := new(controllers.ConfigController)
		system.GET("/config", sc.GetConfig)

		w := new(controllers.WorkerController)
		system.GET("/workers", w.GetList)

		// Статус работающих фоновых задач
		system.GET("/workers/run", w.RunJob)

		// Статус работающих фоновых задач
		system.GET("/workers/status", w.GetStatus)

		// Группа по работе с пользователями
		su := new(controllers.UserController)
		system.GET("users", su.GetUsers)
		system.GET("user/:name", su.GetUserByName)
	}

	return router
}
