package boltdb

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/boltdb/bolt"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"os"
	"path/filepath"
	"time"
)

const (
	USER_DB_BUCKET = "user"
)

var (
	localDb *LocalDbType
)

type LocalDbType struct {
	boltDb    *bolt.DB
	connected bool
}

func ConnectDb() (*LocalDbType, error) {
	// boltdb
	localDb = &LocalDbType{}
	err := localDb.Open()
	if err != nil {
		localDb.connected = false
		return localDb, fmt.Errorf("%s: %s", "Connect to boltDb: ", err.Error())
	}
	localDb.connected = true
	return localDb, nil
}

func GetDb() (*LocalDbType, error) {
	if localDb.connected {
		return localDb, nil
	}
	return nil, fmt.Errorf("Cannot get DB")
}

func (bd *LocalDbType) Open() error {
	fullPath := config.Get().BoltDB.Path
	dir, _ := filepath.Split(fullPath)
	var err error
	if _, err = os.Stat(dir); err != nil {
		err = os.Mkdir(dir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("LocalDbType.Open: %s", err.Error())
		}
	}

	bd.boltDb, err = bolt.Open(fullPath, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		bd.connected = false
		return err
	}
	return nil
}

func (bd *LocalDbType) Close() {
	_ = bd.boltDb.Close()
	bd.connected = false
}

// Получить в Json формате
func (bd *LocalDbType) GetJson(bucketName string, key string, data interface{}) error {
	return bd.boltDb.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket != nil {
			v := bucket.Get([]byte(key))
			if len(v) > 0 {
				err := json.Unmarshal(v, &data)
				if err != nil {
					return err
				}
			}
		}

		return nil
	})
}

// Получить строку
func (bd *LocalDbType) GetStr(bucketName string, key string) (data string, err error) {
	err = bd.boltDb.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket != nil {
			v := bucket.Get([]byte(key))
			data = string(v)
		}
		return nil
	})
	return
}

// Сохранить байты
func (bd *LocalDbType) PutByte(bucketName string, key string, data []byte) error {
	return bd.boltDb.Update(func(tx *bolt.Tx) error {
		// создаем bucket с нужным именем если он еще не создан
		bucket, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		if err != nil {
			return err
		}
		return bucket.Put([]byte(key), data)
	})
}

// Сохранить json строку
func (bd *LocalDbType) PutJson(bucketName string, key string, data interface{}) error {
	// сериализауем в json
	encoded, err := json.Marshal(data)
	if err != nil {
		return err
	}

	return bd.PutByte(bucketName, key, encoded)
}

// Сохранить GOB объект
func (bd *LocalDbType) PutGob(bucketName string, key string, data interface{}) error {
	// сериализуем в gob
	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	err := enc.Encode(data)
	if err != nil {
		return err
	}
	return bd.PutByte(bucketName, key, buf.Bytes())
}

// получить GOB объект
func (bd *LocalDbType) GetGob(bucketName, key string, data interface{}) error {
	return bd.boltDb.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		if bucket != nil {
			v := bucket.Get([]byte(key))
			if len(v) > 0 {
				buf := bytes.NewBuffer(v)
				dec := gob.NewDecoder(buf)
				return dec.Decode(data)
			}
		}
		return nil
	})
}

// удалить
func (bd *LocalDbType) Delete(bucketName, key string) error {
	return bd.boltDb.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		if err != nil {
			return err
		}
		return bucket.Delete([]byte(key))
	})
}

func (bd *LocalDbType) GetBucketList(bucketName string, res map[string][]byte) error {
	return bd.boltDb.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(bucketName))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			res[string(k)] = v
		}
		return nil
	})
}
