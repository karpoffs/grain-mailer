package sql

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"log"
	"strconv"
)

var db *gorm.DB
var err error

func Init() {
	// доступ к структуре конфига
	c := config.Get()

	// Строка соединения
	var connStr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&collation=%s&parseTime=True&loc=Local&multiStatements=true",
		c.Database.User, c.Database.Password,
		c.Database.Host, strconv.FormatUint(uint64(c.Database.Port), 10),
		c.Database.Name, c.Database.Charset, c.Database.Collation)
	db, err = gorm.Open(c.Database.Type, connStr)
	if err != nil {
		log.Fatal(err)
	}

	db.DB().SetMaxIdleConns(0)

	//db.AutoMigrate(&models.Entity{})

	/*
		db.Callback().Query().Before("gorm:query").Register("plugin:cache_data", func(scope *gorm.Scope){
			modelStruct := scope.GetModelStruct()
			fmt.Printf("Model %#v\n", modelStruct.ModelType)
			fmt.Printf("Model %v\n", modelStruct.ModelType)
		})
	*/
}

func Close() error {
	return db.Close()
}

func GetDB() *gorm.DB {
	c := config.Get()

	if c.Environment == "development" {
		return db.Debug().LogMode(true)
	}
	return db
}
