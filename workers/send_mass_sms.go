package workers

import (
	"gitlab.magic-egg.net/server/grain-mailer/service"
	"log"
)

// Job Specific Functions
type SendMassSms struct{}

// will get triggered automatically.
func (f SendMassSms) Run() {

	data, err := service.SendBidsSms() 
	if err != nil {
		log.Println(err)
		return
	}

	// Sends some email
	log.Printf("Sending sms filters %+v \n", data)
}
