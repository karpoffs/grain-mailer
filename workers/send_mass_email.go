package workers

import (
	"gitlab.magic-egg.net/server/grain-mailer/service"
	"log"
)

// Job Specific Functions
type SendMassEmail struct{}

// will get triggered automatically.
func (f SendMassEmail) Run() {

	data, err := service.SendBidsEmail() 
	if err != nil {
		log.Println(err)
		return
	}

	// Sends some email
	log.Printf("Sending emails filters %+v \n", data)
}
