package workers

import (
	"fmt"
	"github.com/robfig/cron"
)

type Service struct {
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Job         cron.Job `json:"-"`
}

var workers = []Service{
	{Name: "LoadDataForEmailsSms", Job: LoadDataForEmailsSms{}, Description: "Load data background"},
	{Name: "sendMassEmail", Job: SendMassEmail{}, Description: "Sending mass emails data background"},
	{Name: "sendMassSms", Job: SendMassSms{}, Description: "Sending mass sms data background"},
}

//GetWorker
func GetWorker(name string) (cron.Job, error) {
	for _, worker := range workers {
		if worker.Name == name {
			return worker.Job, nil
		}
	}
	return nil, fmt.Errorf("service not found %s: %v", name, workers)
}

func GetWorkersCollection() *[]Service {
	return &workers
}
