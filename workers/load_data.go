package workers

import (
	"gitlab.magic-egg.net/server/grain-mailer/service"
	"log"
)

type LoadDataForEmailsSms struct{}

func (f LoadDataForEmailsSms) Run() {
	data, err := service.LoadDataToCache()
	if err != nil {
		log.Println(err)
		return
	}

	// Sends some email
	log.Printf("Load data users: %+v \n", data)
}
