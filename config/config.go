package config

import (
	"github.com/jinzhu/configor"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"log"
	"time"
)

// Конфиг приложения
type Config struct {
	// Настройка окружения
	Environment string `default:"development" env:"APP_ENV" json:"environment"`

	// Настройки http сервиса
	Server struct {
		Address      string        `default:"0.0.0.0" env:"SERVER_ADDRESS" yaml:"address" json:"address"`
		Port         uint          `default:"3000" env:"SERVER_PORT" yaml:"port" json:"port"`
		ReadTimeout  time.Duration `default:"10" yaml:"read_timeout" json:"read_timeout"`
		WriteTimeout time.Duration `default:"10" yaml:"write_timeout" json:"write_timeout"`
	} `json:"server"`

	ViewPath string `default:"views/" yaml:"views_path"  json:"view_path"`

	// Массив пользователей - пара login:password
	Users map[string]string `json:"users"`

	Project struct {
		Base    string `default:"http://localhost/" json:"base" yaml:"base"`
		ApiBase string `yaml:"api_base,omitempty" json:"api_base"`
		Auth    struct {
			Login    string `require:"false" json:"login" yaml:"login,omitempty"`
			Password string `require:"false" json:"password" yaml:"password,omitempty"`
		} `yaml:"auth" json:"auth"`
		CountBestBids uint `yaml:"count_best_bids" json:"count_best_bids"`
		Cache         struct {
			SourceData string `default:"cache" yaml:"source_data" json:"source_data"`
		} `yaml:"cache" json:"cache"`
		Emails map[string]string `yaml:"emails,omitempty" json:"emails"`
	} `yaml:"project,omitempty" json:"project"`

	Database struct {
		Type      string `default:"mysql" yaml:"type,omitempty" json:"type"`
		Host      string `default:"localhost" yaml:"host,omitempty" json:"host"`
		Port      uint   `default:"3306" yaml:"port,omitempty" json:"port"`
		Name      string `default:"demo" yaml:"name,omitempty" json:"name"`
		Charset   string `default:"utf8mb4" yaml:"charset,omitempty" json:"charset"`
		Collation string `default:"utf8mb4_general_ci" env:"DB_COLLATION" json:"collation"`
		User      string `default:"root" env:"DB_USER" json:"user"`
		Password  string `env:"DB_PASSWORD" json:"-"`
	} `yaml:"database" json:"database"`

	BoltDB struct {
		Path string `default:"data/db/bolt.db" yaml:"path,omitempty" json:"path"`
	} `yaml:"boltdb,omitempty" json:"boltdb"`

	Mailers struct {
		Default string                  `yaml:"default" json:"default"`
		Sender  string                  `default:"sender@example.com" yaml:"sender" json:"sender"`
		Pool    map[string]types.Mailer `yaml:"pool" json:"pool"`
	} `yaml:"mailers" json:"mailers"`

	Workers []types.Worker `yaml:"workers,omitempty" json:"workers"`
}

var c = Config{}

// Инициализация конфигурации
func Init(env string, verbose bool, filePath string) {
	if env == "release" {
		env = "production"
	}
	cC := configor.Config{Environment: env, Debug: env == "development", Verbose: verbose}
	err := configor.New(&cC).Load(&c, filePath)
	if err != nil {
		log.Printf("Error: %+v\n", err)
		log.Fatal("error on parsing configuration file")
	}
}

func Get() *Config {
	return &c
}
