package middlewares

import (
	"github.com/gin-gonic/gin"
)

func AuthMiddleware(users *map[string]string) gin.HandlerFunc {
	return gin.BasicAuth(*users)
}
