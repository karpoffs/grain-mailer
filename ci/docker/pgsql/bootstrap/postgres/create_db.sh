#!/usr/bin/env bash

psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
  CREATE USER diary_db WITH SUPERUSER;
EOSQL

psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
  CREATE DATABASE diary_v2 owner diary_db;
EOSQL

# report status
echo ''
echo ''
echo ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'
echo ';;                                                                            ;;'
echo ';;              ---==| POSTGRES INITIALIZATION COMPLETED |==----              ;;'
echo ';;                                                                            ;;'
echo ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'
echo ''
echo ''
