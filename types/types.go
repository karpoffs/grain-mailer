package types

const (
	USER_DB_BUCKET   = "user"
	ENCODE_TYPE_JSON = "json"
	ENCODE_TYPE_GOB  = "gob"
)
