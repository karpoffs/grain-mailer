package types

type Mailer struct {
	Type       string `default:"smtp" yaml:"type" json:"type"`
	Host       string `default:"localhost" yaml:"host" json:"host"`
	Port       uint   `default:"25" yaml:"port" json:"port"`
	User       string `default:"" yaml:"user" json:"user"`
	Domain     string `default:"" yaml:"domain" json:"domain"`
	Password   string `default:"" yaml:"password" json:-`
	PrivateKey string `default:"" yaml:"private_key" json:-`
}
