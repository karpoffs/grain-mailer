package types

type Worker struct {
	Time   string `yaml:"time" json:"time"`
	Worker string `yaml:"worker" json:"worker"`
}
