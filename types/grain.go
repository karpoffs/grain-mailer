package types

import (
	"time"
)

type ApiResponseUser struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    User          `json:"data"`
	Errors  []interface{} `json:"errors"`
	List    []interface{} `json:"list"`
}

type ApiResponseToken struct {
	Token  string           `json:"token"`
}

type ApiResponseMessage struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    string        `json:"data"`
	Errors  []interface{} `json:"errors"`
}

type ApiResponseSettings struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    Settings      `json:"data"`
}

type ApiResponseError struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    []interface{} `json:"data"`
	Errors  []interface{} `json:"errors"`
	List    []interface{} `json:"list"`
	Page    int           `json:"page"`
}

type ApiResponseUsers struct {
	Status     int    `json:"status"`
	Message    string `json:"message"`
	Data       []User `json:"data"`
	Page       int    `json:"page"`
	PerPage    int    `json:"per_page"`
	Total      int    `json:"total"`
	TotalPages int    `json:"total_pages"`
}

type ApiResponseBidFilter struct {
	Status     int         `json:"status"`
	Message    string      `json:"message"`
	Data       FilterData  `json:"data"`
	Page       int         `json:"page"`
	PerPage    int         `json:"per_page"`
	Total      int         `json:"total"`
	TotalPages int         `json:"total_pages"`
}

type ApiResponseBidBest struct {
	Status     int     `json:"status"`
	Message    string  `json:"message"`
	Data       BidBest `json:"data"`
	Page       int     `json:"page"`
	PerPage    int     `json:"per_page"`
	Total      int     `json:"total"`
	TotalPages int     `json:"total_pages"`
}

type Settings struct {
	ManagerEmail				string	`json:"manager_email"`
	ManagerPhone				string	`json:"manager_phone"`
	TrialTariffExpiredMessage	string	`json:"trial_tariff_expired_message"`
	TrialDays					int		`json:"trial_days"`
}

type FunnelState struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Role       string `json:"role"`
	Color      string `json:"color"`
	Engagement int    `json:"engagement"`
}

type Company struct {
	ID        int    `json:"id"`
	ShortName string `json:"short_name"`
	Inn       string `json:"inn"`
}

type Location struct {
	Lat      float64 `json:"lat"`
	Lng      float64 `json:"lng"`
	Country  string  `json:"country"`
	Province string  `json:"province"`
	City     string  `json:"city"`
	Street   string  `json:"street"`
	Text     string  `json:"text"`
	House    string  `json:"house"`
}

type Author struct {
	ID      int     `json:"id"`
	Fio     string  `json:"fio"`
	Login   string  `json:"login"`
	Company Company `json:"company"`
	UseVat  bool    `json:"use_vat"`
}

type Vendor struct {
	ID      	int     `json:"id"`
	Fio     	string  `json:"fio"`
	Login		string  `json:"login"`
	Phone		string  `json:"phone"`
	Company		Company `json:"company"`
	UseVat		bool    `json:"use_vat"`
	Surname		string	`json:"surname"`
	Firstname	string	`json:"firstname"`
	Lastname 	string	`json:"lastname"`
}

type Crop struct {
	ID   int         `json:"id"`
	Name string      `json:"name"`
	Vat  interface{} `json:"vat"`
}

type Parameter struct {
	ID   int      `json:"id"`
	Name string   `json:"name"`
	Type string   `json:"type"`
	Enum []string `json:"enum"`
}

type ParameterValues struct {
	Parameter Parameter   `json:"parameter"`
	Value     interface{} `json:"value"`
}

type ParameterValuesResponse struct {
	Id		  		int		    `json:"id"`
	Value     		interface{} `json:"value"`
	ParameterName	string		`json:"parameter_name"`
}

type Point struct {
	ID       int     `json:"id"`
	Name     string  `json:"name"`
	Lat      float64 `json:"lat"`
	Lng      float64 `json:"lng"`
	Country  string  `json:"country"`
	Province string  `json:"province"`
	City     string  `json:"city"`
	Street   string  `json:"street"`
	House    string  `json:"house"`
	Text     string  `json:"text"`
}

type PointPrice struct {
	Point Point   `json:"point"`
	Price float64 `json:"price"`
}

type PointPrices struct {
	Point              Point   `json:"point"`
	PriceDelivery      float64 `json:"price_delivery"`
	Distance           float64 `json:"distance"`
	PriceDeliveryPerKm float64 `json:"price_delivery_per_km"`
	PriceWithDelivery  float64 `json:"price_with_delivery"`
	Profit             float64 `json:"profit"`
}

type RequestUser struct {
	Login    string `form:"login" xml:"login" json:"login" binding:"required"`
	Password string `form:"password" xml:"password"  json:"password" binding:"required"`
}

type TariffMatrix struct {
	ID                               uint   		`json:"id"`
	Tariff                           Tariff 		`json:"tariff"`
	TariffPeriod					 TariffPeriod	`json:"tariff_period"`
	Role                             Role 			`json:"role"`
	Price							 int			`json:"price"`
	PriorityPlacesBidsCount			 int			`json:"priority_places_bids_count,omitempty"`
	PriorityPlacesBidsOnMailingCount int			`json:"priority_places_bids_on_mailing_count,omitempty"`
	CommonBidsCount                  int    		`json:"common_bids_count,omitempty"`
	MaxFiltersCount                  int    		`json:"max_filters_count,omitempty"`
	MaxCropsCount                    int    		`json:"max_crops_count,omitempty"`
	ContactViewLimit          		 int    		`json:"contact_view_limit,omitempty"`
	MaxSmsCount 					 int    		`json:"max_sms_count,omitempty"`
}

type Tariff struct {
	ID                               uint   `json:"id"`
	Name                             string `json:"name"`
}

type Role struct {
	ID                               uint   `json:"id"`
	Name                             string `json:"name"`
}

type TariffPeriod struct {
	ID                               uint   `json:"id"`
	Period                           int 	`json:"period"`
}

type User struct {
	ID                        uint          `json:"id"`
	Email                     string        `json:"email"`
	APIToken                  string        `json:"api_token"`
	Fio                       string        `json:"fio"`
	Phone                     string        `json:"phone"`
	Login                     string        `json:"login"`
	Status                    string        `json:"status"`
	FunnelState               FunnelState   `json:"funnel_state,omitempty"`
	Location                  Location      `json:"location,omitempty"`
	Points                    []Point       `json:"points,omitempty"`
	Company                   Company       `json:"company,omitempty"`
	CompanyConfirmedByEmail   bool          `json:"company_confirmed_by_email"`
	CompanyConfirmedByPhone   bool          `json:"company_confirmed_by_phone"`
	CompanyConfirmedByPayment bool          `json:"company_confirmed_by_payment"`
	IsBuyer                   bool          `json:"is_buyer"`
	IsAdmin                   bool          `json:"is_admin"`
	IsVendor                  bool          `json:"is_vendor"`
	IsFunnelStateAutomate     bool          `json:"is_funnel_state_automate"`
	UseVat                    bool          `json:"use_vat"`
	IsManager				  bool			`json:"is_manager"`
	IsTrader				  bool			`json:"is_trader"`
	BidFilters                BidFiltersMap `json:"bid_filters,omitempty"`
	Tariff                    Tariff        `json:"tariff,omitempty"`
	Surname					  string		`json:"surname"`
	Firstname				  string		`json:"firstname"`
	Lastname				  string		`json:"lastname"`
	JWTToken				  string
}

func (u *User) GetFilterTypes() []string {
	// Админ получает 2 типа фильтра ("sale", "purchase")
	if u.IsAdmin {
		return []string{"sale", "purchase"}
	}

	// Продавец - один тип на покупку ("sale")
	if u.IsVendor {
		return []string{"purchase"}
	}

	// Покупатель один тип на продажу ("purchase")
	if u.IsBuyer {
		return []string{"sale"}
	}

	return []string{}
}

type BidFiltersMap map[string][]BidFilter

type RequestBestBidParameterValue struct {
	ParameterID int         `json:"parameter_id"`
	Value       interface{} `json:"value"`
}

type RequestBestBidPointPrice struct {
	PointId int     `json:"point_id"`
	Price   float64 `json:"price"`
}

type RequestBestBid struct {
	Filter struct {
		CropID          int                            `json:"cropId"`
		MaxDistance     int                            `json:"max_distance"`
		MaxFullPrice    int                            `json:"max_full_price"`
		MinFullPrice    int                            `json:"min_full_price"`
		ParameterValues []RequestBestBidParameterValue `json:"parameter_values,omitempty"`
		PointPrices     []RequestBestBidPointPrice     `json:"point_prices,omitempty"`
	} `json:"filter"`
}

type FilterData struct {
	Filters         []BidFilter       `json:"filters"`
	FilterCount     int               `json:"filter_count"`
}

type BidFilter struct {
	ID              int               `json:"id"`
	Name            string            `json:"name"`
	Crop            Crop              `json:"crop,omitempty"`
	MaxFullPrice    int               `json:"max_full_price"`
	MinFullPrice    int               `json:"min_full_price"`
	MaxDistance     int               `json:"max_distance"`
	ParameterValues []ParameterValues `json:"parameter_values,omitempty"`
	PointPrices     []PointPrice      `json:"point_prices,omitempty"`
	BidType         string            `json:"bid_type"`
	Subscribed      bool              `json:"subscribed"`
	IsSendingSms	bool			  `json:"is_sending_sms"`
	Purchased		bool			  `json:"purchased"`
	BidBests        []BidBest         `json:"bid_best,omitempty"`
}

type BidBest struct {
	Equal   []Equal   `json:"equal,omitempty"`
	Inexact []Inexact `json:"inexact,omitempty"`
}

type Equal struct {
	ID                       int               			`json:"id"`
	CropID                   int               			`json:"crop_id"`
	Price                    float64           			`json:"price"`
	Volume                   float64           			`json:"volume"`
	Description              string            			`json:"description"`
	Author                   Author            			`json:"author,omitempty"`
	CreatedAt                time.Time         			`json:"created_at"`
	ModifiedAt               time.Time         			`json:"modified_at"`
	Vendor                   Vendor            			`json:"vendor,omitempty"`
	Location                 Location          			`json:"location,omitempty"`
	ParameterValues          []ParameterValuesResponse	`json:"parameter_values,omitempty"`
	PointPrices              []PointPrices     			`json:"point_prices,omitempty"`
	Distance                 int               			`json:"distance"`
	PriceDeliveryPerKm       float64           			`json:"price_delivery_per_km"`
	PriceDelivery            float64           			`json:"price_delivery"`
	PriceWithDelivery        float64           			`json:"price_with_delivery"`
	PriceWithDeliveryWithVat float64           			`json:"price_with_delivery_with_vat"`
	Profit                   float64           			`json:"profit"`
	Vat                      float64           			`json:"vat"`
	Type                     string            			`json:"type"`
	IsPro                    bool              			`json:"is_pro"`
	Tariff                   Tariff            			`json:"tariff,omitempty"`
}

type Inexact struct {
	ID                       int               		   `json:"id"`
	CropID                   int               		   `json:"crop_id"`
	Price                    float64           		   `json:"price"`
	Volume                   float64           		   `json:"volume"`
	Description              string            		   `json:"description"`
	Author                   Author            		   `json:"author,omitempty"`
	CreatedAt                time.Time         		   `json:"created_at"`
	ModifiedAt               time.Time         		   `json:"modified_at"`
	Vendor                   Vendor            		   `json:"vendor,omitempty"`
	Location                 Location          		   `json:"location,omitempty"`
	ParameterValues          []ParameterValuesResponse `json:"parameter_values,omitempty"`
	PointPrices              []PointPrices     		   `json:"point_prices,omitempty"`
	Distance                 int               		   `json:"distance"`
	PriceDeliveryPerKm       float64           		   `json:"price_delivery_per_km"`
	PriceDelivery            float64           		   `json:"price_delivery"`
	PriceWithDelivery        float64           		   `json:"price_with_delivery"`
	PriceWithDeliveryWithVat float64           		   `json:"price_with_delivery_with_vat"`
	Profit                   float64           		   `json:"profit"`
	Vat                      float64           		   `json:"vat"`
	Type                     string            		   `json:"type"`
	IsPro                    bool              		   `json:"is_pro"`
	Tariff                   Tariff            		   `json:"tariff,omitempty"`
}
