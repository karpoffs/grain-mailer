package types

type Message struct {
	From      string `default:"noreply@example.com"`
	Subject   string `default:"Hello!"`
	Body      string `default:"Hello World!"`
	BodyHtml  string `default:"<html><body>Hello World!</body></html>"`
	Recipient string `default:"example@example.com"`
}
