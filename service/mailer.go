package service

import (
	"context"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"log"
	"time"

	"github.com/mailgun/mailgun-go/v4"
)

type MailerService struct {
}

func (s MailerService) Send(msg *types.Message) (bool, error) {

	c := config.Get()
	def := c.Mailers.Default
	pool := c.Mailers.Pool
	m := pool[def]

	// Create an instance of the Mailgun Client
	mg := mailgun.NewMailgun(m.Domain, m.PrivateKey)

	// Set mailgun region host api
	mg.SetAPIBase(m.Host)

	// The message object allows you to add attachments and Bcc recipients
	message := mg.NewMessage(msg.From, msg.Subject, msg.Body, msg.Recipient)
	message.SetHtml(msg.BodyHtml)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	// Send the message	with a 10 second timeout
	resp, id, err := mg.Send(ctx, message)
	if err != nil {
		log.Printf("Response %+v, Error %+v, Recipient %+v", resp, err, msg.Recipient)
		return false, err
	}

	log.Printf("ID: %s Resp: %s\n", id, resp)
	return len(id) > 0 && len(resp) > 0, nil
}
