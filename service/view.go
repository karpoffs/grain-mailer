package service

import (
	"fmt"
	"github.com/flosch/pongo2"
	. "github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"net/http"
	"os"
	"path/filepath"
)

// may also use an absolute path:
var root, _ = os.Getwd()

func RenderTemplate(name string, data map[string]interface{}) (string, error) {

	fullPath := filepath.Join(root, config.Get().ViewPath+name)

	if _, err := os.Stat(fullPath); os.IsNotExist(err) {
		return "", err
	}

	template := pongo2.Must(pongo2.FromFile(fullPath))
	res, err := template.Execute(convertContext(data))

	if err != nil {
		return "", err
	}

	return res, nil
}

func Pongo2() HandlerFunc {
	return func(c *Context) {
		c.Next()

		//name, err := stringFromContext(c, "template")
		name, ok := c.Get("template")
		if ok {
			fullPath := filepath.Join(root, config.Get().ViewPath+name.(string))

			if _, err := os.Stat(fullPath); os.IsNotExist(err) {
				http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
			}

			data, _ := c.Get("data")
			template := pongo2.Must(pongo2.FromFile(fullPath))
			err := template.ExecuteWriter(convertContext(data), c.Writer)
			if err != nil {
				http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
			}
		}
	}
}

func stringFromContext(c *Context, input string) (string, error) {
	raw, ok := c.Get(input)
	if ok {
		strVal, ok := raw.(string)
		if ok {
			return strVal, nil
		}
	}
	return "", fmt.Errorf("No data for context variable: %s", input)
}

func convertContext(thing interface{}) pongo2.Context {
	if thing != nil {
		context, isMap := thing.(map[string]interface{})
		if isMap {
			return context
		}
	}
	return nil
}
