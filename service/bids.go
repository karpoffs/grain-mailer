package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.magic-egg.net/server/grain-mailer/config"
	"gitlab.magic-egg.net/server/grain-mailer/database/boltdb"
	"gitlab.magic-egg.net/server/grain-mailer/domain"
	"gitlab.magic-egg.net/server/grain-mailer/types"
	"log"
	"net/url"
	"strconv"
	"time"
)

func LoadDataToCache() (map[string]bool, error) {
	store, err := boltdb.GetDb()
	if err != nil {
		return nil, err
	}

	usersEmail, usersSms, err := domain.GetDataAllUsersFromApiService()
	if err != nil {
		return nil, err
	}

	var result = make(map[string]bool)

	//Сохраняем новых пользователей
	for _, user := range usersEmail {
		key := strconv.Itoa(int(user.ID))
		//log.Printf("%s - %v \n", key, user)
		//err := store.PutGob(types.USER_DB_BUCKET, key, user)
		err := store.PutJson(types.USER_DB_BUCKET, key, user)
		if err != nil {
			result[key] = false
		}
		result[key] = true
	}

	for _, user := range usersSms {
		key := strconv.Itoa(int(user.ID))
		//log.Printf("%s - %v \n", key, user)
		//err := store.PutGob(types.USER_DB_BUCKET, key, user)
		err := store.PutJson(types.USER_DB_BUCKET, key, user)
		if err != nil {
			result[key] = false
		}
		result[key] = true
	}
	return result, nil
}

func SendBidsEmail() (map[string]bool, error) {

	c := config.Get()
	var store *boltdb.LocalDbType
	var err error
	var users []types.User
	var usersEmail []types.User
	var usersPurchasedEmail []types.User
	var settings types.Settings

	store, err = boltdb.GetDb()
	if err != nil {
		return nil, err
	}

	if c.Project.Cache.SourceData == "cache" {
		users, err = domain.GetUserListFromDB(store, types.USER_DB_BUCKET, types.ENCODE_TYPE_JSON)
		for _, user := range users {
			var isEmailSubscribed = false
			var isPurchased = false

			for _, filterType := range user.GetFilterTypes() {
				for _, bidFilter := range user.BidFilters[filterType] {
					if bidFilter.Subscribed == true {
						isEmailSubscribed = true
					}
					if bidFilter.Purchased == true {
						isPurchased = true
					}
				}
			}
			if isEmailSubscribed == true {
				if isPurchased == true {
					usersPurchasedEmail = append(usersPurchasedEmail, user)
				} else {
					usersEmail = append(usersEmail, user)
				}
			}
		}
		usersEmail = append(usersPurchasedEmail, usersEmail...)
	} else {
		usersEmail, _, err = domain.GetDataAllUsersFromApiService()
	}

	if err != nil {
		return nil, err
	}

	settings, err = domain.RequestSettings()

	var result = make(map[string]bool)
	mailer := new(MailerService)

	for _, user := range usersEmail {
		if user.Email != "" {
			filterType := user.GetFilterTypes()[0]

			if len(user.BidFilters[filterType]) > 0 {
				bidFilter := user.BidFilters[filterType][0]

				if len(bidFilter.BidBests) > 0 {
					if len(bidFilter.BidBests[0].Equal) > 0 {
						key := strconv.Itoa(int(user.ID))

						log.Printf(
							"Email to %+v, price %v, deliveryPrice %v",
							user.Email,
							bidFilter.BidBests[0].Equal[0].Price,
							bidFilter.BidBests[0].Equal[0].PriceWithDelivery)

						dataTmpl := gin.H{
							"item":         bidFilter.BidBests[0].Equal[0],
							"cropName":     bidFilter.Crop.Name,
							"project_base": config.Get().Project.Base,
							"user":         user,
							"settings":     settings}
						htmlString, err := RenderTemplate("email/bids.html", dataTmpl)
						if err != nil {
							return nil, err
						}

						subject, ok := c.Project.Emails["Bids"]
						if !ok {
							subject = ""
						}

						var mes = types.Message{
							From:      c.Mailers.Sender,
							Subject:   subject,
							BodyHtml:  htmlString,
							Recipient: user.Email,
						}

						ok, _ = mailer.Send(&mes)
						log.Printf("Sending email to %+v, status %v", user.Email, ok)
						result[key] = ok
						time.Sleep(10 * time.Second)
					}
				}
			}
		}
	}

	return result, nil
}

func SendBidsSms() (map[string]bool, error) {

	c := config.Get()
	var store *boltdb.LocalDbType
	var err error
	var users []types.User
	var usersSms []types.User

	store, err = boltdb.GetDb()
	if err != nil {
		return nil, err
	}

	if c.Project.Cache.SourceData == "cache" {
		users, err = domain.GetUserListFromDB(store, types.USER_DB_BUCKET, types.ENCODE_TYPE_JSON)
		for _, user := range users {
			var isSmsSubscribed = false

			for _, filterType := range user.GetFilterTypes() {
				for _, bidFilter := range user.BidFilters[filterType] {
					if bidFilter.IsSendingSms == true {
						isSmsSubscribed = true
					}
				}
			}
			if isSmsSubscribed == true {
				usersSms = append(usersSms, user)
			}
		}
	} else {
		_, usersSms, err = domain.GetDataAllUsersFromApiService()
	}

	if err != nil {
		return nil, err
	}

	var result = make(map[string]bool)
	for _, user := range usersSms {
		key := strconv.Itoa(int(user.ID))
		for _, filterType := range user.GetFilterTypes() {
			for _, bidFilter := range user.BidFilters[filterType] {
				price := bidFilter.BidBests[0].Equal[0].PriceWithDelivery
				if int(price) == 0 {
					price = bidFilter.BidBests[0].Equal[0].Price
				}
				cropId := bidFilter.Crop.ID
				vendor := bidFilter.BidBests[0].Equal[0].Vendor
				route := filterType + "/best-bids/" + fmt.Sprintf("%d", cropId)
				urlString := c.Project.Base + route

				u, _ := url.Parse(urlString)
				queryString := u.Query()
				queryString.Set("bearer", user.JWTToken)
				u.RawQuery = queryString.Encode()

				var message string
				var vendorName string
				if len(vendor.Company.ShortName) == 0 {
					vendorName = vendor.Surname
					message = vendorName + " готов "
				} else {
					vendorName = vendor.Company.ShortName
					message = vendorName + " готовы "
				}
				log.Printf("Sending sms to %+v\n", vendor.Phone)
				if (user.IsBuyer || user.IsManager || user.IsAdmin) {
					message = message + "продать " + bidFilter.Crop.Name + " с доставкой до вашего склада по " + fmt.Sprintf("%.0f", price) + " руб."
				}

				if user.IsVendor {
					message = message + "купить " + bidFilter.Crop.Name + " по " + fmt.Sprintf("%.0f", price) + " руб. у вас на воротах"
				}

				if vendor.Phone != "" {
					message = message + ", +" + vendor.Phone
				}
				message = message + " Подробнее: " + u.String()

				log.Printf("Sending sms to %+v\n", user.Phone)
				ok, _ := domain.RequestSendSmsToUser(user, message)
				result[key] = ok
			}
		}
	}

	return result, nil
}

func SendBidsByUser(user types.RequestUser) (map[string]bool, error) {

	var result = make(map[string]bool)

	c := config.Get()

	usersEmail, _, err := domain.GetDataByOneUser(user)
	if err != nil {
		return nil, err
	}

	mailer := new(MailerService)

	for _, user := range usersEmail {

		dataTmpl := gin.H{"items": user.BidFilters, "project_base": config.Get().Project.Base, "user": user}
		htmlString, err := RenderTemplate("email/bids.html", dataTmpl)
		if err != nil {
			return nil, err
		}
		subject, ok := c.Project.Emails["Bids"]
		if !ok {
			subject = ""
		}
		var mes = types.Message{
			From:      c.Mailers.Sender,
			Subject:   subject,
			BodyHtml:  htmlString,
			Recipient: user.Email,
		}

		ok, _ = mailer.Send(&mes)
		result[user.Email] = ok
	}

	return result, nil
}

func PreviewBidsByUser(user types.RequestUser) (string, error) {

	usersEmail, _, err := domain.GetDataByOneUser(user)
	if err != nil {
		return "", err
	}

	for _, user := range usersEmail {
		dataTmpl := gin.H{"items": user.BidFilters, "project_base": config.Get().Project.Base}
		htmlString, err := RenderTemplate("email/bids.html", dataTmpl)
		if err != nil {
			return "", err
		}
		return htmlString, nil
	}
	return "", nil
}
